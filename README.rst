======================
Primo Embedded Search
======================

Description
===========
A demo for a new Primo embeddded search that includes search tabs for books, articles and media.  Each tab when searched will generate a custom url that links to the ci-collections scope and imposes limits using Primo search facets within the url.

.. note:: 

    Demo can be seen at https://jsbdev.cikeys.com/apps/demo/librarysearch/

    A bitbucket repository is also available https://bitbucket.org/mruiz019/primo-embedded

URL Configuration
=================

``base url:`` https://ci-primo.hosted.exlibrisgroup.com/primo-explore/search

Common Parameters
-----------------

The below table lists parameters that should be included with all searches.  Each tab will have an additional set of parameters to include alongside these

**Parameters**

============ =========================
Param        Value
============ =========================
institution  01CALS_UCI
vid          01CALS_UCI
mode         advanced
query        any,contains, ``<query>``
search_scope 01CALS_UCI
tabs         books_local
============ =========================

Book Tab
--------

The books tab searches print materials within the library, invoking the 'available in library' facet.

**Parameters**

===== =====================================
Param Value
===== =====================================
facet tlevel,include,available$$I01CALS_UCI
===== =====================================

Articles Tab
------------

The article tab limits the search to articles by invoking the filter for articles within the advanced search dropdown

**Parameters**

======= =====================================
Param   Value
======= =====================================
pfilter pfilter,exact,articles,AND
======= =====================================

Media Tab
---------

The media tab limits the search to media items by setting the 'available in library' and 'audiovisual' facets.

**Parameters**

===== =====================================
Param Value
===== =====================================
facet rtype,include,media
facet tlevel,include,available$$I01CALS_UCI
===== =====================================
