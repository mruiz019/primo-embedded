/**
 * Created by marco.ruiz on 1/18/2017.
 */
'use strict';

var default_tab_id = "books";
var active_tab;

var tab_map = {
    "articles": ["articles_tab"],
    "books": ["books_tab"],
    "media": ["books_tab", "media_tab"]        
    };

// intialize default active tab on page load
if(active_tab == "" || active_tab == null || active_tab == 'undefined'){
    document.getElementById(default_tab_id).classList.add('active');
    active_tab = default_tab_id;
    }
function reset_page(){
//for caching problems
    // reset the query inputs to disabled
    for(var key in tab_map){
        for(var index in tab_map[key]){
            document.getElementById(tab_map[key][index]).setAttribute("disabled", "disabled");
            }
        }
    // set query input to empty string
    document.getElementById('query').value = "";
    }

// set active tab css
function activeTab(element) {
    document.getElementById(active_tab).classList.remove('active');
    element.classList.add('active');
    active_tab = element.id;
    }

function searchPrimo() {
    // reset page before submission
    reset_page();

    //populate query to send
    document.getElementById('query').value = "any,contains, " + document.getElementById('searchBox').value;    
    
    var facets = tab_map[active_tab];
    
    for(var index in facets){
        document.getElementById(facets[index]).removeAttribute('disabled');
        }
               
    // submit form
    document.getElementById('searchForm').submit();
    }

function searchKeyPress(e) {
    if (typeof e == 'undefined' && window.event) {
        e = window.event;
        } 
    if (e.keyCode == 13) {
        document.getElementById('searchBox').click();
        }
    } 

